using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy_Move : Base_Behaviour
{
    private bool _move;

    private Vector3 _current_Position;

    private NavMeshAgent _agent;

    protected override void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
    }

    public void Init(float speed)
    {
        _agent.speed = speed;
    }

    protected override void Update()
    {
        Move();
    }

    public void Move()
    {
        if (!_move)
        {
            _current_Position = Get_Sphere_Point(15f);
            _agent.SetDestination(_current_Position);
            _move = true;
        }
        else
        {
            float distanceToTarget = Vector3.Distance(transform.position, _current_Position);

            if (distanceToTarget < _agent.stoppingDistance + 1)
            {
                _move = false;
            }
        }
    }


    private Vector3 Get_Sphere_Point(float range)
    {
        Vector3 randomPoint = transform.position + Random.insideUnitSphere * range;

        if (NavMesh.SamplePosition(randomPoint, out NavMeshHit hit, 100.0f, NavMesh.AllAreas))
        {
            return hit.position;
        }

        return Vector3.zero;
    }
}
