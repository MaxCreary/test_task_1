using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Base_Behaviour
{
    public Enemy_Type enemy_Type;

    private float _health = 10.0f;

    private Enemy_Move _enemy_Move;
    private Game_Behaviour _game_Behaviour;

    protected override void Awake()
    {
        _enemy_Move = GetComponent<Enemy_Move>();
    }

    public void Init(float health, float speed, Game_Behaviour game_Behaviour)
    {
        _health = health;
        _game_Behaviour = game_Behaviour;

        _enemy_Move.Init(speed);
    }

    public void Apply_Damage(float damage)
    {
        _health -= damage;

        if(_health <= 0.0f)
        {
            _game_Behaviour.Enemy_Delete(this);
            Destroy(gameObject);
        }
    }
}

public enum Enemy_Type
{
    Easy,
    Medium,
    Hard
}
