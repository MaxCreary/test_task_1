using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemys_Spawn : Base_Behaviour
{
    [SerializeField]
    private List<Enemy> _enemys;
    [SerializeField]
    private Game_Behaviour _game_Behaviour;

    private float _time_Spawn;
    private float _time_Min;
    private float _time_Max;
    private float _time;
    private float _enemy_Speed;

    private BoxCollider _box_Collider;

    protected override void Awake()
    {
        _box_Collider = GetComponent<BoxCollider>();
    }

    public void Init(float time_Min, float time_Max, float speed)
    {
        _time_Min = time_Min;
        _time_Max = time_Max;

        _enemy_Speed = speed;

        _time_Spawn = Random.Range(_time_Min, _time_Max);
    }

    protected override void Update()
    {
        if (_game_Behaviour.Play_Game && !_game_Behaviour.Freeze_Game)
            Spawn_Enemy();
    }

    private void Spawn_Enemy()
    {
        _time += Time.deltaTime;

        if (_time > _time_Spawn)
        {
            Enemy enemy = Instantiate(Get_Random_Enemy(), Get_Random_Point(), Quaternion.identity);
            enemy.Init(Random.Range(5, 25), _enemy_Speed, _game_Behaviour);

            _game_Behaviour.Enemy_Add(enemy);

            _time_Spawn = Random.Range(_time_Min, _time_Max);
            _time = 0f;
        }
    }


    private Enemy Get_Random_Enemy()
    {
        int random = Random.Range(0, _enemys.Count - 1);

        return _enemys[random];
    }
    private Vector3 Get_Random_Point()
    {
        float offsetX = Random.Range(-_box_Collider.bounds.size.x , _box_Collider.bounds.size.x);
        float offsetZ = Random.Range(-_box_Collider.bounds.size.z, _box_Collider.bounds.size.z);

        Vector3 point = new Vector3(offsetX, 0, offsetZ);

        if (NavMesh.SamplePosition(point, out NavMeshHit hit, 100.0f, NavMesh.AllAreas))
        {
            return hit.position;
        }

        return Vector3.zero;
    }
}
