using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Menu_Base
{
    protected readonly Panel_Menu _menu;
    protected readonly Panel_Credits _credits;
    protected readonly Panel_Liderboard _liderboard;

    protected readonly IMenu_State _state_Change;

    protected Menu_Base(Panel_Menu menu, Panel_Credits credits, Panel_Liderboard liderboard, IMenu_State state_Change)
    {
        _menu = menu;
        _credits = credits;
        _liderboard = liderboard;

        _state_Change = state_Change;
    }

    /// <summary>
    /// Start current state
    /// </summary>
    public abstract void Start();

    /// <summary>
    /// Stop current state
    /// </summary>
    public abstract void Stop();
}
