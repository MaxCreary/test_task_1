using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Menu_Behaviour : Base_Behaviour, IMenu_State
{
    [SerializeField]
    private Game_Behaviour _game_Behaviour;
    [SerializeField]
    private UI_Behaviour _ui_Behaviour;


    [SerializeField] 
    private Panel_Menu _menu;
    [SerializeField] 
    private Panel_Credits _credits;
    [SerializeField] 
    private Panel_Liderboard _liderboard;

    private Menu_Base _current_State;
    private List<Menu_Base> _all_States;

    protected override void Start()
    {
        base.Start();

        _all_States = new List<Menu_Base>()
        {
            new State_Menu(_menu, _credits, _liderboard, this),
            new State_Game(_menu, _credits, _liderboard, this),
            new State_Credits(_menu, _credits, _liderboard, this),
            new State_Liderboard(_menu, _credits, _liderboard, this)
        };

        _current_State = _all_States[0];
    }

    #region UI Button
    public void Button_Play()
    {
        _ui_Behaviour.Show();

        _game_Behaviour.Init(true);
        _ui_Behaviour.Init();

        Change_State<State_Game>();
    }
    public void Button_Menu()
    {
        _ui_Behaviour.Hide();

        _game_Behaviour.Init(false);

        Change_State<State_Menu>();
    }


    public void Button_Credits()
    {
        Change_State<State_Credits>();
    }
    public void Button_Liderboard()
    {
        Change_State<State_Liderboard>();
    }
    public void Button_Back()
    {
        Change_State<State_Menu>();
    }
    public void Button_Exit()
    {
        Application.Quit();
    }
    #endregion

    public void Check_Liderboard()
    {
        _liderboard.Check_Liderboard();
    }

    public void Change_State<T>() where T : Menu_Base
    {
        var state = _all_States.FirstOrDefault(s => s is T);
        _current_State.Stop();
        state.Start();
        _current_State = state;
    }
}
