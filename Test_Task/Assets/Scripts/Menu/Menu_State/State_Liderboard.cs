using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State_Liderboard : Menu_Base
{
    public State_Liderboard(Panel_Menu menu, Panel_Credits credits, Panel_Liderboard liderboard, IMenu_State state_Change)
    : base(menu, credits, liderboard, state_Change)
    {
    }

    public override void Start()
    {
        _liderboard.Show();
    }
    public override void Stop()
    {
        _liderboard.Hide();
    }
}