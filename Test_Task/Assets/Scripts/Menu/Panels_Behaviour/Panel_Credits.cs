using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel_Credits : Base_Behaviour
{
    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
