using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Panel_Liderboard : Base_Behaviour
{
    [SerializeField]
    private Text _text;

    protected override void OnEnable()
    {
        if (!PlayerPrefs.HasKey("Liderboard"))
            PlayerPrefs.SetInt("Liderboard", 0);

        _text.text = PlayerPrefs.GetInt("Liderboard").ToString();
    }

    public void Check_Liderboard()
    {
        if (!PlayerPrefs.HasKey("Liderboard"))
            PlayerPrefs.SetInt("Liderboard", 0);

        int liderboard = PlayerPrefs.GetInt("Liderboard");
        int score = PlayerPrefs.GetInt("Score");

        if(score > liderboard)
        {
            PlayerPrefs.SetInt("Liderboard", score);
            _text.text = score.ToString();
        }
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
