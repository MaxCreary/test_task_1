using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMenu_State
{
    void Change_State<T>() where T : Menu_Base;
}
