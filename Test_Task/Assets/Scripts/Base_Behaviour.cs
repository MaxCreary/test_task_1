using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base_Behaviour : MonoBehaviour
{
    protected virtual void Awake() { }
    protected virtual void OnEnable() { }
    protected virtual void OnDisable() { }
    protected virtual void Start() { }
    protected virtual void Update() { }
    protected virtual void LateUpdate() { }
    protected virtual void FixedUpdate() { }
}
