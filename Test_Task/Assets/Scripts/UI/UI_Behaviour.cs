using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Behaviour : Base_Behaviour
{
    [SerializeField]
    private Menu_Behaviour _menu_Behaviour;

    [SerializeField]
    private UI_Score_Game _ui_Score_Game;
    [SerializeField]
    private UI_Failed _ui_Failed;
    [SerializeField]
    private List<UI_Bonus> _ui_Bonus;

    public void Init()
    {
        _ui_Score_Game.Init();

        for (int x = 0; x < _ui_Bonus.Count; x++)
            _ui_Bonus[x].Bonus_Restart();
    }

    public void Add_Score()
    {
        _ui_Score_Game.Add_Score();
    }

    public void Game_Failed()
    {
        StartCoroutine(nameof(Open_Menu));

        _ui_Score_Game.Set_Save_Score();
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }

    private IEnumerator Open_Menu()
    {
        _ui_Failed.Show();

        yield return new WaitForSeconds(2f);

        _ui_Failed.Hide();

        _menu_Behaviour.Button_Menu();
        _menu_Behaviour.Check_Liderboard();
    }
}
