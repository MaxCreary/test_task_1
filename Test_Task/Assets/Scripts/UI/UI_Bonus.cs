using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Bonus : Base_Behaviour
{
    private Button _button;

    protected override void Awake()
    {
        _button = GetComponent<Button>();
    }

    public void Bonus_Restart()
    {
        _button.interactable = true;
    }
}
