using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Score_Game : Base_Behaviour
{
    [SerializeField]
    private List<Level> _levels;

    [SerializeField]
    private Enemys_Spawn _spawn;


    [SerializeField]
    private Text _text;

    private int _score;

    public void Init()
    {
        _score = 0;
        _text.text = _score.ToString();

        Add_Level(0);
    }

    public void Add_Score()
    {
        _score++;

        _text.text = _score.ToString();

        switch (_score)
        {
            case 10:
                Add_Level(1);
                break;

            case 20:
                Add_Level(2);
                break;
        }
    }

    private void Add_Level(int count)
    {
        _spawn.Init(_levels[count]._time_Min,
                    _levels[count]._time_Max,
                    _levels[count]._enemy_Speed);
    }


    public void Set_Save_Score()
    {
        PlayerPrefs.SetInt("Score", _score);
    }
}

[System.Serializable]
public struct Level
{
    public float _time_Min;
    public float _time_Max;
    public float _enemy_Speed;
}