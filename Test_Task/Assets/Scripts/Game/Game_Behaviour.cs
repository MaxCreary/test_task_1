using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Behaviour : Base_Behaviour
{
    [SerializeField]
    private UI_Behaviour _ui_Behaviour;

    private List<Enemy> _enemys_Created = new List<Enemy>();

    public bool Play_Game { get; private set; }
    public bool Freeze_Game { get; private set; }

    public void Init(bool play)
    {
        Play_Game = play;
    }

    #region Bonus Game
    public void Bonus_Boom()
    {
        for (int x = 0; x < _enemys_Created.Count; x++)
        {
            _enemys_Created[x].Apply_Damage(999.0f);
        }
    }
    public void Bonus_Freeze()
    {
        Freeze_Game = !Freeze_Game;

        Invoke(nameof(Bonus_Freeze), 3.0f);
    }
    #endregion

    public void Enemy_Add(Enemy enemy)
    {
        _enemys_Created.Add(enemy);

        Check_Amount();
    }
    public void Enemy_Delete(Enemy enemy)
    {
        _enemys_Created.Remove(enemy);

        _ui_Behaviour.Add_Score();
    }

    private void Check_Amount()
    {
        // Failed
        if (_enemys_Created.Count >= 10)
        {
            Play_Game = false;

            for (int x = 0; x < _enemys_Created.Count; x++)
            {
                Destroy(_enemys_Created[x].gameObject);
            }

            _enemys_Created.Clear();

            _ui_Behaviour.Game_Failed();
        }
    }
}
