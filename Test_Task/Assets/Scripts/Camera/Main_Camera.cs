using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main_Camera : Base_Behaviour
{
    [SerializeField]
    private float _damage = 10.0f;

    [SerializeField]
    private Game_Behaviour game_Behaviour;

    protected override void Update()
    { 
        if (game_Behaviour.Play_Game)
            Mouse_Click();
    }

    private void Mouse_Click()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                if (hit.collider.gameObject.TryGetComponent(out Enemy enemy))
                {
                    enemy.Apply_Damage(_damage);
                }
            }
        }
    }
}
